import argparse
import cmd
import shlex
import textwrap
import types
from functools import wraps


def debug(*args, **kwargs):
    if True:
        print(*args, **kwargs)


def yn(prompt, default="n"):
    c = ["y", "n"]
    default = default.lower()
    assert default in c
    c[c.index(default)] = c[c.index(default)].upper()
    prompt += " ({})".format("/".join(c))
    return (input(prompt) or default).lower()[0] == "y"


class Exit(Exception):
    pass


class PatchedArgumentParser(argparse.ArgumentParser):

    def exit(self, status=0, message=None):
        if message:
            raise RuntimeError(message)
        else:
            raise RuntimeError


class Args_cmd(cmd.Cmd):

    def get_names(self):
        names = dir(self)
        print(self.modules)
        for module, (modobj, functions) in self.modules.items():
            for funcname, funcobj in functions:
                print(funcname)
                names += [funcname]
        return names

    def build_argparser(self, func):
        if hasattr(func, "parser_name") and hasattr(func, "parser_args"):
            debug("Building argparser for {}".format(func.parser_name))
            debug("-" * 5)
            for k, v in (self.subparsers.choices).items():
                debug(k, v)
            debug("-" * 5)
            for k, v in (self.subparsers._name_parser_map).items():
                debug(k, v)
            debug("-" * 5)
            func.parser = self.subparsers.add_parser(
                func.parser_name,
                formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                prog=func.parser_name,
                description=getattr(func, "__doc__", None)
            )
            debug("Adding arguments:")
            for parser_args, parser_kwargs in func.parser_args:
                debug("\t{}".format(parser_args))
                func.parser.add_argument(*parser_args, **parser_kwargs)
            debug("Patching docstring for {}".format(func.parser_name))
            func.__doc__ += '\nrun "{} -h" for more help'.format(
                func.parser_name)
            setattr(func, "completer", self.build_completer(func))

    def __new__(cls, *args, **kwargs):
        """Patch argument parser into methods"""
        if cls.__doc__:
            cls.intro = textwrap.dedent(cls.__doc__).strip()
        cls.modules = {}
        cls.func_backups = {}
        cls.prompt = "{}>".format(cls.__name__)
        cls.parser = PatchedArgumentParser(cls.__name__)
        cls.subparsers = cls.parser.add_subparsers(dest="command")
        cls.do_help.__doc__ = cmd.Cmd.do_help.__doc__
        for name in dir(cls):
            if name.startswith("do_"):
                cls.build_argparser(cls, getattr(cls, name))
        return super().__new__(cls, *args, **kwargs)

    @staticmethod
    def argparse(*args):
        """add argument parser to command"""
        def wrapper(f):
            if not f.__name__.startswith("do_"):
                raise RuntimeError(
                    "{} is not a do_* function!".format(f.__name__))
            f.parser_name = f.__name__[3:]
            f.parser_args = args
            debug("Building argparse wrapper for {}".format(f.parser_name))

            @wraps(f)
            def wrapped(self, args):
                args = [f.parser_name] + shlex.split(args)
                try:
                    args = self.parser.parse_args(args)
                except RuntimeError as e:
                    if not str(e):
                        return
                    e_msg = str(e).split(wrapped.__name__[
                        3:] + ": ")[-1].split("error: ", 1)[1]
                    debug("Error:", e_msg)
                    return
                return f(self, args)
            return wrapped
        return wrapper

    @staticmethod
    def build_completer(func):
        """Construct Tab-Completer for command"""
        return lambda *args, **kwargs: []
        debug("Building completer for {}".format(func.parser_name))

        def completer(self, prefix, line, begin_idx, end_idx):
            sline = list(filter(bool, line.split(" ")))
            optio_parser = func.parser._option_string_actions
            options = list(func.parser._option_string_actions.keys())
            completions = []
            if prefix:
                completions = list(filter(lambda x: x.startswith(prefix), options))
            if completions:
                debug(completions)
                debug()
                return completions
            for pos in range(1, len(sline) + 1):
                opt = sline[-pos]
                if opt == "--":
                    break
                if opt in optio_parser:
                    debug(pos, optio_parser[opt].nargs)
                    debug()
                    if pos <= optio_parser[opt].nargs:
                        choices = optio_parser[opt].choices or []
                        return list(filter(lambda s: s.startswith(prefix), choices))
            return []
        return completer

    @argparse.__func__(
        [("modules",), {'type': str, 'nargs': '+', 'default': [],
                        'metavar':'module', 'action':'store', 'help':'modules to load'}]
    )
    def do_load(self, args):
        """load Modules"""
        #make_method=lambda f:wraps(f)(lambda *args,**kwargs:f(self,*args,**kwargs))
        make_method = lambda f: types.MethodType(f, self)
        for module in args.modules:
            try:
                module = __import__(module)
            except Exception as e:
                debug("Error importing modules '{}': {}".format(module, e))
                continue
            for name, func in vars(module).items():
                if hasattr(func, '__bases__'):
                    if func.__bases__ == type(self).__bases__:
                        if name in self.modules:
                            debug("Module '{}' already loaded".format(name))
                            continue
                        self.modules[name] = (func, [])
                        print("Loading Module '{}'".format(name))
                        for funcname, funcobj in vars(self.modules[name][0]).items():
                            if funcname.startswith("do_"):
                                if hasattr(self, funcname):
                                    print("command {} already define".format(
                                        funcname[3:]))
                                    if yn("Overwrite?"):
                                        if funcname not in self.func_backups:
                                            self.func_backups[funcname] = (name, getattr(self, funcname))
                                            delattr(self.__class__, funcname)
                                    else:
                                        continue
                                debug("defining method", funcname[3:])
                                self.build_argparser(funcobj)
                                self.modules[name][1].append((funcname, make_method(funcobj)))

    def do_modules(self, args):
        """Print Loaded Modules"""
        print("Loaded Modules:")
        for module, (modobj, functions) in self.modules.items():
            print("{}:".format(module))
            for funcname, funcobj in functions:
                print("\t{}".format(funcname))

    @argparse.__func__(
        [("modules",), {'type': str, 'nargs': '+', 'default': [], 'metavar':'module', 'action':'store', 'help':'modules to load'}]
    )
    def do_unload(self, args):
        """Unload Modules"""
        for to_unload in args.modules:
            for module, (modobj, functions) in self.modules.copy().items():
                if module == to_unload or modobj.__module__ == to_unload:
                    debug("unloading module {}:".format(module), modobj)
                    del self.modules[module]
                    for fname, (modname, function) in self.func_backups.copy().items():
                        if modname == module:
                            debug("restoring function", fname)
                            if fname in self.func_backups:
                                mod, function = self.func_backups[fname]
                                setattr(self, fname, function)
                                del self.func_backups[fname]
                    break

    def do_EOF(self, args):
        """Exit"""
        return self.do_exit(args)

    def do_exit(self, args):
        """Exit"""
        if hasattr(self, "quitmsg") and self.quitmsg:
            print(self.quitmsg)
        return True

    def __getattr__(self, attr):
        for module, (modobj, functions) in self.modules.items():
            for funcname, funcobj in functions:
                if funcname == attr:
                    print(attr, "found in module", module)
                    return funcobj
        raise AttributeError("'{}' object has no attribute '{}'".format(type(self).__name__, attr))

    def default(self, args):
        args = shlex.split(args)
        print("Command not found: {}".format(args[0]))

    def emptyline(self):
        return
