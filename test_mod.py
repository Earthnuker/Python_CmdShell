from collections import namedtuple

from cmd_args import Args_cmd

vec3 = namedtuple('vec3', ['x', 'y', 'z'])


class Module_Test(Args_cmd):
    """
    Test Module
    type 'help or ?' for help
    """
    @Args_cmd.argparse(
        [("-F", "--foo",), {'type': float, 'nargs': '*', 'metavar': 'Foo',
                            'action': 'store', 'help': 'foos to bar (float)'}],
        [("-B", "--bar"), {'type': lambda x: int(x, 16), 'nargs': '+',
                           'metavar': 'Bar', 'action': 'store', 'help': 'bars to foo (hex)'}],
        [("-T", "--target",), {'type': float, 'nargs': 3, 'default': vec3(0, 0, 0), 'metavar': (
            'x', 'y', 'z'), 'action': 'store', 'help': 'where to put the result'}],
        [("-m", "--mode",), {'type': str, 'nargs': 1, 'default': "normal", 'metavar': 'mode',
                             'action': 'store', 'choices': ('normal', 'extra', 'ultra'), 'help': 'set processing mode'}],
        [("baz",), {'type': str, 'nargs': '+', 'default': [0],
                    'metavar':'Baz', 'action':'store', 'help':'bazes to foobar'}]
    )
    def do_test(self, args):
        """foo bars and bar foos"""
        args.target = vec3(*args.target)
        print(args)

    @Args_cmd.argparse(
        [("-s", "--silent"), {'action': 'store_true', 'help': 'be quiet'}]
    )
    def do_bar_fuzz(self, args):
        """fuzzes bars"""
        print(args)
