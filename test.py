from collections import namedtuple

from cmd_args import Args_cmd


class App(Args_cmd):
    """
    Test App
    type 'help or ?' for help
    """
    quitmsg = "bye"
    prompt = "lel>"

    @Args_cmd.argparse(
        [("--hurr",), {'default': False, 'action': 'store_true',
                       'help': 'hurr the durr instead'}],
        [("herp",), {'type': str, 'nargs': '+', 'default': [],
                     'metavar':'Herp', 'action':'store', 'help':'herps to derp'}]
    )
    def do_test(self, args):
        """herp the derp"""
        print(vars(args))

a = App()
autoexec = [
    "help",
    "help test",
    "load test_mod",
    "help",
    "help test",
    "modules",
    "test -h",
    "help bar_fuzz",
    "bar_fuzz -h",
    "unload test_mod",
    "help",
    "help test",
    "modules",
]

for line in autoexec:
    print()
    print("{}{}".format(a.prompt, line))
    a.onecmd(line)
#print("-----")
#a.cmdloop()
